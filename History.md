# grunt-tyops Release History

## v0.1.1 / 2023-05-20
* Switch CI to GitLab CI and repo references to GitLab (James D. Forrester)
* build: Drop unused Travis CI config (James D. Forrester)
* build: Update for the first time in six years(!) (James D. Forrester)
* build: Upgrade eslint-config-wikimedia dev-dependency to 0.25.0 (James D. Forrester)
* build: Upgrade grunt dev-dependency to 1.6.1 (James D. Forrester)
* tyops.js: Drop old jshint comment (#6) (James D. Forrester)

## v0.1.0 / 2015-07-15
* Initial release version (James D. Forrester)

## v0.0.1 / 2014-11-18
* Initial version, inside OOjs UI (James D. Forrester)
